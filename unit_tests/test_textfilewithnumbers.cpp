//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "textfile.h"
#include "TextFileWithNumbers.h"

#include <fstream>
#include <sstream>


class TestTextFileWithNumbers : public ::testing::Test {
protected:
    TextFile sourceFile;

    TestTextFileWithNumbers()
        : sourceFile("source_numbers.txt") {}

    void SetUp() override {
    }

    void TearDown() override {
        sourceFile.remove();
    }

    void fill_source_with_numbers(const std::string &header, const std::initializer_list<int> &numbers) {
        std::stringstream ss;
        ss << header << std::endl;
        for (auto const& number : numbers) {
            ss << number << std::endl;
        }

        sourceFile.setContent(ss.str());
    }

    void fill_source_with_numbers(const std::initializer_list<int> &numbers) {
        std::stringstream ss;
        for (auto const& number : numbers) {
            ss << number << std::endl;
        }

        sourceFile.setContent(ss.str());
    }

    std::vector<int> nums(const std::initializer_list<int> &numbers) {
        return numbers;
    }
};


TEST_F(TestTextFileWithNumbers, types_can_read_numbers_from_file) {
    fill_source_with_numbers({ 2, 5, 4, 1 });
    TextFileWithNumbers numbersFile(sourceFile.path());

    ASSERT_TRUE(numbersFile.read());

    ASSERT_EQ(std::string(), numbersFile.getHeader());
    ASSERT_EQ(nums({ 2, 5, 4, 1 }), numbersFile.getNumbers());
}

TEST_F(TestTextFileWithNumbers, types_can_read_file_with_header_and_numbers) {
    fill_source_with_numbers("sort alg", { 2, 5, 4, 1 });
    TextFileWithNumbers numbersFile(sourceFile.path());

    ASSERT_TRUE(numbersFile.read());

    ASSERT_EQ(std::string("sort alg"), numbersFile.getHeader());
    ASSERT_EQ(nums({ 2, 5, 4, 1 }), numbersFile.getNumbers());
}

TEST_F(TestTextFileWithNumbers, types_can_read_empty_file) {
    fill_source_with_numbers({ });
    TextFileWithNumbers numbersFile(sourceFile.path());

    ASSERT_TRUE(numbersFile.read());
}

TEST_F(TestTextFileWithNumbers, types_cannot_read_if_file_not_exists) {
    TextFileWithNumbers numbersFile(sourceFile.path());

    ASSERT_FALSE(numbersFile.read());
}

TEST_F(TestTextFileWithNumbers, types_cannot_write_file_with_wrong_name) {
    TextFileWithNumbers numbersFile("some_wrong_name_/?*.txt");

    ASSERT_FALSE(numbersFile.write());
}

TEST_F(TestTextFileWithNumbers, types_can_write_file_with_numbers) {
    TextFileWithNumbers numbersFile(sourceFile.path());

    numbersFile.setNumbers({ 1, 2, 3 });
    numbersFile.write();

    ASSERT_EQ(std::string("1\n2\n3\n"), sourceFile.getContent());
}

TEST_F(TestTextFileWithNumbers, types_can_write_file_with_header_and_numbers) {
    TextFileWithNumbers numbersFile(sourceFile.path());

    numbersFile.setHeader("some sort alg");
    numbersFile.setNumbers({ 5, 4, 1 });
    numbersFile.write();

    ASSERT_EQ(std::string("some sort alg\n5\n4\n1\n"), sourceFile.getContent());
}
