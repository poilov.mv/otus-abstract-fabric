//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "BubbleSortFabric.h"
#include "BubbleSortAlgorithm.h"


TEST(TestBubbleSortFabric, can_create_bubble_sort) {
    BubbleSortFabric fabric;
    auto algorithm = fabric.create();

    auto rightType = dynamic_cast<BubbleSortAlgorithm*>(algorithm.get());
    ASSERT_TRUE(rightType);
}

TEST(TestBubbleSortFabric, correct_name_of_olgorithm) {
    BubbleSortFabric fabric;

    ASSERT_EQ(std::string("bubble"), fabric.type());
}
