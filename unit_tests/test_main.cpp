//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "main_app.h"
#include "TextFileWithNumbers.h"
#include "textfile.h"

#include <algorithm>

struct IntGenerator {
    int operator()() {
        return (rand() % 1000) - 500;
    }
};


class TestMain : public ::testing::Test {
protected:
    std::string sorttype;
    std::string sourcePath;
    std::string targetPath;

    void SetUp() override {
        sorttype = "selection";
        sourcePath = "source.txt";
        targetPath = "target.txt";
    }

    void TearDown() override {
        TextFile(sourcePath).remove();
        TextFile(targetPath).remove();
    }

    std::vector<int> generate(int count) {
        std::vector<int> sourceNumbers;
        sourceNumbers.resize(count);
        std::generate_n(sourceNumbers.begin(), count, IntGenerator());
        return sourceNumbers;
    }

    int run_main(
        const std::string &sortType = std::string(),
        const std::string &sourceFilePath = std::string(),
        const std::string &targetFilePath = std::string()
        ) {
        std::string pr("program");
        std::string st(sortType);
        std::string sp(sourceFilePath);
        std::string tp(targetFilePath);

        int count = 1
            + (!st.empty() ? 1 : 0)
            + (!sp.empty() ? 1 : 0)
            + (!tp.empty() ? 1 : 0);

        char *argv[] = { &pr.at(0), 
            st.empty() ? NULL : &st.at(0), 
            sp.empty() ? NULL : &sp.at(0), 
            tp.empty() ? NULL : &tp.at(0) };
        return main_app(count, argv);
    }
};


TEST_F(TestMain, types_sort_numbers_from_source_to_target_file) {
    TextFileWithNumbers sourceFile(sourcePath);
    sourceFile.setNumbers(generate(50));
    sourceFile.write();

    ASSERT_EQ(0, run_main(sorttype, sourcePath, targetPath));

    TextFileWithNumbers targetFile(targetPath);
    targetFile.read();

    std::vector<int> expected(sourceFile.getNumbers());
    std::sort(expected.begin(), expected.end());

    ASSERT_FALSE(targetFile.getHeader().empty());
    ASSERT_EQ(expected, targetFile.getNumbers());
}

TEST_F(TestMain, show_synopsis_and_returns_error_without_args) {
    TextFileWithNumbers sourceFile(sourcePath);
    sourceFile.setNumbers(generate(50));
    sourceFile.write();

    ASSERT_EQ(1, run_main());
}

TEST_F(TestMain, returns_error_with_wrong_source_file) {
    ASSERT_EQ(2, run_main(sorttype, sourcePath, targetPath));
}

TEST_F(TestMain, returns_error_with_wrong_target_file) {
    TextFileWithNumbers sourceFile(sourcePath);
    sourceFile.setNumbers(generate(50));
    sourceFile.write();

    targetPath = "wrong_file_name_?*/.txt";
    ASSERT_EQ(2, run_main(sorttype, sourcePath, targetPath));
}
