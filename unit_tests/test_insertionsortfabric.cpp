//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "InsertionSortFabric.h"
#include "InsertionSortAlgorithm.h"


TEST(TestInsertionSortFabric, can_create_insertion_sort) {
    InsertionSortFabric fabric;
    auto algorithm = fabric.create();

    auto rightType = dynamic_cast<InsertionSortAlgorithm*>(algorithm.get());
    ASSERT_TRUE(rightType);
}

TEST(TestInsertionSortFabric, correct_name_of_olgorithm) {
    InsertionSortFabric fabric;

    ASSERT_EQ(std::string("insertion"), fabric.type());
}
