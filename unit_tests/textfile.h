#pragma once

#include <fstream>
#include <sstream>


class TextFile {
    std::string filepath;

public:
    TextFile(const std::string &filepath);

    std::string getContent();

    void setContent(const std::string &value);

    void remove();

    std::string path() const;
};
