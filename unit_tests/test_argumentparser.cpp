//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "ArgumentParser.h"


class TestArgumentParser : public ::testing::Test {
protected:
    ArgumentParser createParser(
        const std::string &arg1 = std::string(),
        const std::string &arg2 = std::string(),
        const std::string &arg3 = std::string()
    ) {
        std::string p("program");
        std::string a1(arg1);
        std::string a2(arg2);
        std::string a3(arg3);

        int count = 1
            + (!a1.empty() ? 1 : 0)
            + (!a2.empty() ? 1 : 0)
            + (!a3.empty() ? 1 : 0);

        char *argv[] = { &p.at(0),
            a1.empty() ? NULL : &a1.at(0),
            a2.empty() ? NULL : &a2.at(0),
            a3.empty() ? NULL : &a3.at(0) };

        return ArgumentParser(count, argv);
    }
};

TEST_F(TestArgumentParser, can_parse_three_args) {
    auto parser = createParser("type", "source", "target");

    ASSERT_TRUE(parser.isArgsValid());
}

TEST_F(TestArgumentParser, cant_parse_not_three_args) {
    auto parser = createParser("type", "source");

    ASSERT_FALSE(parser.isArgsValid());
}

TEST_F(TestArgumentParser, can_get_type) {
    auto parser = createParser("type", "source", "target");

    ASSERT_EQ(std::string("type"), parser.sorterType());
}

TEST_F(TestArgumentParser, can_get_source_file) {
    auto parser = createParser("type", "source", "target");

    ASSERT_EQ(std::string("source"), parser.sourceFilePath());
}

TEST_F(TestArgumentParser, can_get_target_file) {
    auto parser = createParser("type", "source", "target");

    ASSERT_EQ(std::string("target"), parser.targetFilePath());
}

TEST_F(TestArgumentParser, can_get_synopsis) {
    auto parser = createParser("type", "source", "target");

    auto synopsis = parser.synopsis({ "type" });

    ASSERT_FALSE(synopsis.empty());
}

TEST_F(TestArgumentParser, can_synopsis_contains_types) {
    auto parser = createParser("type", "source", "target");
    
    auto synopsis = parser.synopsis({ "one type", "another type" });

    ASSERT_TRUE(synopsis.find("one type") != std::string::npos);
    ASSERT_TRUE(synopsis.find("another type") != std::string::npos);
}
