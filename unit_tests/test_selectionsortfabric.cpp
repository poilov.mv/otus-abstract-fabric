//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "SelectionSortFabric.h"
#include "SelectionSortAlgorithm.h"


TEST(TestSelectionSortFabric, can_create_selection_sort) {
    SelectionSortFabric fabric;
    auto algorithm = fabric.create();

    auto rightType = dynamic_cast<SelectionSortAlgorithm*>(algorithm.get());
    ASSERT_TRUE(rightType);
}

TEST(TestSelectionSortFabric, correct_name_of_olgorithm) {
    SelectionSortFabric fabric;

    ASSERT_EQ(std::string("selection"), fabric.type());
}
