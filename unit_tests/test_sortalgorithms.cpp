//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "SortAlgorithms.h"

#include "SelectionSortAlgorithm.h"
#include "InsertionSortAlgorithm.h"
#include "BubbleSortAlgorithm.h"

#include "SelectionSortFabric.h"
#include "InsertionSortFabric.h"
#include "BubbleSortFabric.h"

#include <algorithm>


TEST(TestSortAlgorithms, types_contains_selection_fabric) {
    SelectionSortFabric fabric;
    SortAlgorithms sorters;
    auto types = sorters.types();

    auto exists = std::find(types.begin(), types.end(), fabric.type()) != types.end();

    ASSERT_TRUE(exists);
}

TEST(TestSortAlgorithms, types_contains_insertion_fabric) {
    InsertionSortFabric fabric;
    SortAlgorithms sorters;
    auto types = sorters.types();

    auto exists = std::find(types.begin(), types.end(), fabric.type()) != types.end();

    ASSERT_TRUE(exists);
}

TEST(TestSortAlgorithms, types_contains_bubble_fabric) {
    BubbleSortFabric fabric;
    SortAlgorithms sorters;
    auto types = sorters.types();

    auto exists = std::find(types.begin(), types.end(), fabric.type()) != types.end();

    ASSERT_TRUE(exists);
}

TEST(TestSortAlgorithms, can_create_selection_sort) {
    SelectionSortFabric fabric;
    SortAlgorithms sorters;
    auto algorithm = sorters.create(fabric.type());

    auto rightType = dynamic_cast<SelectionSortAlgorithm*>(algorithm.get());
    ASSERT_TRUE(rightType);
}

TEST(TestSortAlgorithms, can_create_insertion_sort) {
    InsertionSortFabric fabric;
    SortAlgorithms sorters;
    auto algorithm = sorters.create(fabric.type());

    auto rightType = dynamic_cast<InsertionSortAlgorithm*>(algorithm.get());
    ASSERT_TRUE(rightType);
}

TEST(TestSortAlgorithms, can_create_bubble_sort) {
    BubbleSortFabric fabric;
    SortAlgorithms sorters;
    auto algorithm = sorters.create(fabric.type());

    auto rightType = dynamic_cast<BubbleSortAlgorithm*>(algorithm.get());
    ASSERT_TRUE(rightType);
}

TEST(TestSortAlgorithms, wrong_type_returns_any_sorter) {
    SortAlgorithms sorters;
    
    auto algorithm = sorters.create("something wrong");

    ASSERT_TRUE(algorithm);
}
