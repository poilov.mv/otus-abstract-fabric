//
// Created by Maxim on 14.11.2020.
//

#include "gtest/gtest.h"

#include "InsertionSortAlgorithm.h"

class TestInsertionSortAlgorithm : public ::testing::Test {
protected:
    void sort_and_check_numbers(
        const std::initializer_list<int> &source,
        const std::initializer_list<int> &expected) {
        InsertionSortAlgorithm sorter;

        std::vector<int> numbers(source);
        sorter.sort(numbers);

        ASSERT_EQ(std::vector<int>(expected), numbers);
    }
};

TEST_F(TestInsertionSortAlgorithm, can_sort_two_numbers) {
    sort_and_check_numbers(
        { 10, 2 },
        { 2, 10 });
}

TEST_F(TestInsertionSortAlgorithm, do_nothing_if_sorted) {
    sort_and_check_numbers(
        { 1, 5 },
        { 1, 5 });
}

TEST_F(TestInsertionSortAlgorithm, can_sort_three_numbers_v1) {
    sort_and_check_numbers(
        { 3, 1, 2 },
        { 1, 2, 3 });
}

TEST_F(TestInsertionSortAlgorithm, can_sort_three_numbers_v2) {
    sort_and_check_numbers(
        { 2, 1, 3 },
        { 1, 2, 3 });
}

TEST_F(TestInsertionSortAlgorithm, can_sort_three_numbers_v3) {
    sort_and_check_numbers(
        { 3, 2, 1 },
        { 1, 2, 3 });
}

TEST_F(TestInsertionSortAlgorithm, can_sort_three_numbers_v4) {
    sort_and_check_numbers(
        { 2, 3, 1 },
        { 1, 2, 3 });
}

TEST_F(TestInsertionSortAlgorithm, can_sort_three_numbers_v5) {
    sort_and_check_numbers(
        { 2, 3, 1 },
        { 1, 2, 3 });
}

TEST_F(TestInsertionSortAlgorithm, can_sort_with_repeaded_values) {
    sort_and_check_numbers(
        { 7, 2, 7, 1, 6, 8 },
        { 1, 2, 6, 7, 7, 8 });
}

TEST_F(TestInsertionSortAlgorithm, can_sort_10_numbers) {
    sort_and_check_numbers(
        { 7, 6, 1, 3, 4, 8, 9, 45, 34, 5 },
        { 1, 3, 4, 5, 6, 7, 8, 9, 34, 45 });
}
