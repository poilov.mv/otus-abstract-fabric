#include "textfile.h"

#include <fstream>
#include <sstream>


TextFile::TextFile(const std::string &filepath)
    : filepath(filepath) {}

std::string TextFile::getContent() {
    std::ifstream stream(filepath);

    if (!stream.is_open()) {
        std::stringstream ss;
        ss << "Can not open file for reading: " << filepath;
        throw std::runtime_error(ss.str());
    }

    std::stringstream ss;
    std::string line;
    while (std::getline(stream, line)) {
        ss << line << std::endl;
    }

    return ss.str();
}

void TextFile::setContent(const std::string &value) {
    std::ofstream stream(filepath);

    if (!stream.is_open()) {
        std::stringstream ss;
        ss << "Can not open file for writing: " << filepath;
        throw std::runtime_error(ss.str());
    }

    stream << value;
}

void TextFile::remove() {
    std::remove(filepath.c_str());
}

std::string TextFile::path() const {
    return filepath;
}
