#pragma once

#include <string>
#include <vector>

class ArgumentParser {
    std::vector<std::string> args;

public:
    ArgumentParser(int argc, char **argv);

    bool isArgsValid() const;

    std::string sorterType() const;
    std::string sourceFilePath() const;
    std::string targetFilePath() const;

    std::string synopsis(const std::vector<std::string> &sorterTypes) const;
};
