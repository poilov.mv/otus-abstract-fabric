#pragma once

#include "ISortAlgorithm.h"

class InsertionSortAlgorithm
    : public ISortAlgorithm {
public:
    void sort(std::vector<int> &numbers) override;
};
