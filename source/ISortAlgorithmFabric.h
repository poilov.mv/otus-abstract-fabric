#pragma once

#include "ISortAlgorithm.h"
#include <string>
#include <memory>

class ISortAlgorithmFabric {
public:
    virtual ~ISortAlgorithmFabric() = default;

    virtual std::unique_ptr<ISortAlgorithm> create() = 0;

    virtual std::string type() const = 0;
};
