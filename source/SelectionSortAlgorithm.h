#pragma once

#include "ISortAlgorithm.h"

class SelectionSortAlgorithm
    : public ISortAlgorithm {
public:
    void sort(std::vector<int> &numbers) override;
};
