#include "InsertionSortFabric.h"
#include "InsertionSortAlgorithm.h"

std::unique_ptr<ISortAlgorithm> InsertionSortFabric::create() {
    return std::make_unique<InsertionSortAlgorithm>();
}

std::string InsertionSortFabric::type() const {
    return "insertion";
}
