#include "BubbleSortAlgorithm.h"


void BubbleSortAlgorithm::sort(std::vector<int> &numbers) {
    for (int j = 1; j < int(numbers.size()); ++j) {
        bool swapped = false;
        for (int i = 0; i < int(numbers.size())-j; ++i) {
            if (numbers[i] > numbers[i+1]) {
                std::swap(numbers[i], numbers[i+1]);
                swapped = true;
            }
        }
        if (!swapped) {
            break;
        }
    }
}
