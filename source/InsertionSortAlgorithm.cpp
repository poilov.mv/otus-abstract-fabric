#include "InsertionSortAlgorithm.h"


void InsertionSortAlgorithm::sort(std::vector<int> &numbers) {
    for (int i = 1; i < int(numbers.size()); ++i) {
        auto x = numbers[i];
        auto j = i;
        while (j > 0 && numbers[j-1] > x) {
            numbers[j] = numbers[j-1];
            --j;
        }
        numbers[j] = x;
    }
}
