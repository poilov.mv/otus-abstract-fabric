#pragma once

#include "ISortAlgorithmFabric.h"


class SortAlgorithms {
    typedef std::unique_ptr<ISortAlgorithmFabric> ISortAlgorithmFabricUPtr;

    std::vector<ISortAlgorithmFabricUPtr> items;

public:
    SortAlgorithms();

    std::vector<std::string> types() const;

    std::unique_ptr<ISortAlgorithm> create(const std::string &type);
};
