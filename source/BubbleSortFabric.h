#pragma once

#include "ISortAlgorithmFabric.h"


class BubbleSortFabric
    : public ISortAlgorithmFabric {
public:
    std::unique_ptr<ISortAlgorithm> create() override;

    std::string type() const override;
};
