#include "TextFileWithNumbers.h"

#include <fstream>
#include <iostream>


TextFileWithNumbers::TextFileWithNumbers(const std::string &filepath)
    : filepath(filepath) {}

bool TextFileWithNumbers::read() {
    std::ifstream stream(filepath);

    if (!stream.is_open()) {
        return false;
    }

    std::vector<int> result;
    std::string line;

    int number;
    if (stream >> number) {
        result.push_back(number);
        while (stream >> number) {
            result.push_back(number);
        }
    }
    else {
        stream.clear();

        std::getline(stream, line);
        while (stream >> number) {
            result.push_back(number);
        }
    }

    header = line;
    numbers.swap(result);

    return true;
}

bool TextFileWithNumbers::write() {
    std::ofstream stream(filepath);

    if (!stream.is_open()) {
        return false;
    }

    if (!header.empty()) {
        stream << header << std::endl;
    }
    for (auto const& number : numbers) {
        stream << number << std::endl;
    }

    return true;
}

void TextFileWithNumbers::setNumbers(const std::vector<int> &value) {
    numbers = value;
}

std::vector<int> TextFileWithNumbers::getNumbers() const {
    return numbers;
}

void TextFileWithNumbers::setHeader(const std::string &value) {
    header = value;
}

std::string TextFileWithNumbers::getHeader() const {
    return header;
}
