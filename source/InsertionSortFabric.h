#pragma once

#include "ISortAlgorithmFabric.h"

class InsertionSortFabric
    : public ISortAlgorithmFabric {
public:
    std::unique_ptr<ISortAlgorithm> create() override;

    std::string type() const override;
};
