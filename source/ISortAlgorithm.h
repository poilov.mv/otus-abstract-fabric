#pragma once

#include <vector>


class ISortAlgorithm {
public:
    virtual ~ISortAlgorithm() = default;

    virtual void sort(std::vector<int> &numbers) = 0;
};
