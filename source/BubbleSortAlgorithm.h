#pragma once

#include "ISortAlgorithm.h"


class BubbleSortAlgorithm
    : public ISortAlgorithm {
public:
    void sort(std::vector<int> &numbers) override;
};
