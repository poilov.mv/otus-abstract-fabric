#include "SortAlgorithms.h"

#include "SelectionSortFabric.h"
#include "InsertionSortFabric.h"
#include "BubbleSortFabric.h"

#include <algorithm>
#include <cctype>


SortAlgorithms::SortAlgorithms() {
    items.emplace_back(std::make_unique<SelectionSortFabric>());
    items.emplace_back(std::make_unique<InsertionSortFabric>());
    items.emplace_back(std::make_unique<BubbleSortFabric>());
}

std::vector<std::string> SortAlgorithms::types() const {
    std::vector<std::string> result;
    result.reserve(items.size());
    for (auto const& item : items) {
        result.push_back(item->type());
    }
    return result;
}

std::unique_ptr<ISortAlgorithm> SortAlgorithms::create(const std::string &type) {
    using namespace std;

    auto lower_type(type);
    transform(begin(lower_type), end(lower_type), begin(lower_type),
        [](unsigned char c) { return std::tolower(c); });

    auto it = find_if(begin(items), end(items), [type](ISortAlgorithmFabricUPtr const& item) {
        return item->type() == type;
    });
    if (it != end(items)) {
        return (*it)->create();
    }

    return items[0]->create();
}