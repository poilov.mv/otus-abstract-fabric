#pragma once

#include <vector>
#include <string>

class TextFileWithNumbers {
    std::string filepath;
    std::string header;
    std::vector<int> numbers;

public:
    TextFileWithNumbers(const std::string &filepath);

    bool read();
    bool write();

    void setNumbers(const std::vector<int> &value);
    std::vector<int> getNumbers() const;

    void setHeader(const std::string &value);
    std::string getHeader() const;
};
