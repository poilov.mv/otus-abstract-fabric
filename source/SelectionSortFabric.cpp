#include "SelectionSortFabric.h"
#include "SelectionSortAlgorithm.h"


std::unique_ptr<ISortAlgorithm> SelectionSortFabric::create() {
    return std::make_unique<SelectionSortAlgorithm>();
}

std::string SelectionSortFabric::type() const {
    return "selection";
}
