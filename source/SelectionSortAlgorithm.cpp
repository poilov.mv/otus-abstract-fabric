#include "SelectionSortAlgorithm.h"


void SelectionSortAlgorithm::sort(std::vector<int> &numbers) {
    for (int idx_i = 0; idx_i < numbers.size() - 1; idx_i++) {
        int min_idx = idx_i;

        for (int idx_j = idx_i + 1; idx_j < numbers.size(); idx_j++) {
            if (numbers[idx_j] < numbers[min_idx]) {
                min_idx = idx_j;
            }
        }

        if (min_idx != idx_i) {
            std::swap(numbers[idx_i], numbers[min_idx]);
        }
    }
}
