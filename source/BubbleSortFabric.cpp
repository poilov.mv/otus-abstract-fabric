#include "BubbleSortFabric.h"

#include "BubbleSortAlgorithm.h"


std::unique_ptr<ISortAlgorithm> BubbleSortFabric::create() {
    return std::make_unique<BubbleSortAlgorithm>();
}

std::string BubbleSortFabric::type() const {
    return "bubble";
}
