#include "main_app.h"

#include "ArgumentParser.h"
#include "TextFileWithNumbers.h"
#include "SortAlgorithms.h"

#include <iostream>
#include <sstream>


int main_app(int argc, char **argv) {
    SortAlgorithms sorterFabric;

    auto args = ArgumentParser(argc, argv);
    if (args.isArgsValid()) {
        auto reader = std::make_unique<TextFileWithNumbers>(args.sourceFilePath());
        auto writer = std::make_unique<TextFileWithNumbers>(args.targetFilePath());
        auto sorter = sorterFabric.create(args.sorterType());

        if (!reader->read()) {
            std::cout << "Can not open file for reading: " << args.sourceFilePath();
            return 2;
        }

        auto numbers = reader->getNumbers();

        sorter->sort(numbers);

        writer->setHeader(args.sorterType());
        writer->setNumbers(numbers);

        if (!writer->write()) {
            std::cout << "Can not open file for writing: " << args.targetFilePath();
            return 2;
        }

        return 0;
    } else {
        std::cout << args.synopsis(sorterFabric.types());
        return 1;
    }
}
