[![pipeline status](https://gitlab.com/poilov.mv/otus-abstract-fabric/badges/master/pipeline.svg)](https://gitlab.com/poilov.mv/otus-abstract-fabric/-/commits/master)
[![coverage report](https://gitlab.com/poilov.mv/otus-abstract-fabric/badges/master/coverage.svg)](https://gitlab.com/poilov.mv/otus-abstract-fabric/-/commits/master)

# Шаблоны проектирования

## Домашнее задание по теме **Фабричный метод и абстрактная фабрика**
Реализация выбора подходящего метода сортировки (выбором, вставки, слиянием) набора данных с использованием абстрактной фабрики и описание применения шаблона в проекте 
**Цель:** Получите навык работы с абстрактной фабрикой.
Данные задаются в файле. Результат также помещается в файл. 
1. Выбрать массив размером 50 элементов.
2. Создать программу, которая в качестве входного параметра получает вариант сортировки (выбором, вставки, слиянием), имя файла со входным набором данных и имя файла с выходными данными.
3. Реализовать в программе абстрактную фабрику и конкретные фабрики, отвечающие за каждый вариант сортировки как продукты.
4. Программа записывает результаты в выходной файл данных. В содержании в пишется тип сортировки и результаты. 
5. Если потребуется использовать абстрактную фабрику или фабричный метод в проектной работе, предоставить описание в текстовом файле в GitHub репозитории где конкретно и в какой роли используется этот шаблон.
6. нарисовать диаграмму классов.

## Диаграмма классов
### Наследование
```mermaid
classDiagram
    class SelectionSortFabric {
      +create() ISortAlgorithm
      +type() string
    }
    class InsertionSortFabric {
      +create() ISortAlgorithm
      +type() string
    }
    class BubbleSortFabric {
      +create() ISortAlgorithm
      +type() string
    }
    class ISortAlgorithmFabric {
       +create() ISortAlgorithm
       +type() string
     }

    SelectionSortFabric --|> ISortAlgorithmFabric
    InsertionSortFabric --|> ISortAlgorithmFabric
    BubbleSortFabric --|> ISortAlgorithmFabric

    ISortAlgorithm <|-- SelectionSortAlgorithm
    ISortAlgorithm <|-- InsertionSortAlgorithm
    ISortAlgorithm <|-- BubbleSortAlgorithm

    ISortAlgorithmFabric ..> ISortAlgorithm : использует

    class ISortAlgorithm {
      +sort(numbers)
    }
    class SelectionSortAlgorithm {
      +sort(numbers)
    }
    class InsertionSortAlgorithm {
      +sort(numbers)
    }
    class BubbleSortAlgorithm {
      +sort(numbers)
    }
```
### Создание
```mermaid
classDiagram
    class SelectionSortFabric {
      +create() ISortAlgorithm
      +type() string
    }
    class InsertionSortFabric {
      +create() ISortAlgorithm
      +type() string
    }
    class BubbleSortFabric {
      +create() ISortAlgorithm
      +type() string
    }

    SelectionSortFabric --> SelectionSortAlgorithm : создает
    InsertionSortFabric --> InsertionSortAlgorithm : создает
    BubbleSortFabric --> BubbleSortAlgorithm : создает

    class SelectionSortAlgorithm {
      +sort(numbers)
    }
    class InsertionSortAlgorithm {
      +sort(numbers)
    }
    class BubbleSortAlgorithm {
      +sort(numbers)
    }
```
### Вспомогательные классы
```mermaid
classDiagram
  ISortAlgorithmFabric <.. SortAlgorithms : list<ISortAlgorithmFabric> items

  class SortAlgorithms {
    +create(type) ISortAlgorithm
    +types() strings
  }

  SortAlgorithms *-- SelectionSortFabric
  SortAlgorithms *-- InsertionSortFabric
  SortAlgorithms *-- BubbleSortFabric

  class ArgumentParser {
    list<string> args
    +isArgsValid() bool
    +sorterType() string
    +sourceFilePath() string
    +targetFilePath() string
    +synopsis() string
  }

  class TextFileWithNumbers {
    string filepath
    string header
    list<int> numbers

    +TextFileWithNumbers(filepath)
    +setHeader(header)
    +getHeader()
    +setNumbers(numbers)
    +getNumbers()
    +read() bool
    +write() bool
  }
```

## Исходные файлы
### Основное приложение
* **source/main.cpp** 
* **source/main_app.h** 
* **source/main_app.cpp** 
### Интерфейсы
* **source/ISortAlgorithm.h** 
* **source/ISortAlgorithmFabric.h** 
### Реализация алгоритмов сортировки
* **source/SelectionSortAlgorithm.h** 
* **source/SelectionSortAlgorithm.cpp** 
* **source/InsertionSortAlgorithm.h** 
* **source/InsertionSortAlgorithm.cpp** 
* **source/BubbleSortAlgorithm.h** 
* **source/BubbleSortAlgorithm.cpp** 
### Реализация фабрик алгоритмов сортировки
* **source/SelectionSortFabric.h** 
* **source/SelectionSortFabric.cpp** 
* **source/InsertionSortFabric.h** 
* **source/InsertionSortFabric.cpp** 
* **source/BubbleSortFabric.h** 
* **source/BubbleSortFabric.cpp** 
### Класс разбора аргументов командной строки
* **source/ArgumentParser.h** 
* **source/ArgumentParser.cpp** 
### Класс агрегатор списка фабрик алгоритмов сортировки
* **source/SortAlgorithms.h** 
* **source/SortAlgorithms.cpp** 
### Класс обертка для чтения и сохранения текстового файла
* **source/TextFileWithNumbers.h** 
* **source/TextFileWithNumbers.cpp** 
### Тесты
* **unit_tests/test_argumentparser.cpp**
* **unit_tests/test_bubblesortalgorithm.cpp**
* **unit_tests/test_bubblesortfabric.cpp**
* **unit_tests/test_insertionsortalgorithm.cpp**
* **unit_tests/test_insertionsortfabric.cpp**
* **unit_tests/test_main.cpp**
* **unit_tests/test_selectionsortalgorithm.cpp**
* **unit_tests/test_selectionsortfabric.cpp**
* **unit_tests/test_sortalgorithms.cpp**
* **unit_tests/test_textfilewithnumbers.cpp**
### Вспомогательные файлы для тестов
* **unit_tests/textfile.h**
* **unit_tests/textfile.cpp**
